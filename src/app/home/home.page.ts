import { Component, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { analytics } from 'firebase';

import { mmi } from 'mapmyindia-map-cordova-ionic-beta';

declare var google;


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit {

  // @ViewChild('map', { static: true }) mapElement: ElementRef;

  map: any;
  address: string;
  public marker: any = []
  latitude: any;
  longitude: any;
  position: any;

  constructor(
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    // private maps: mmi
  ) { }

  ngOnInit() {


    // this.map = this.maps.loadMaps('map', { key: 'hghezprcj7tvyo3nup21vb4sksf457al', zoom: { control: true }, location: { control: true, initial: true, bounds: true } });

    console.log(document.getElementById('map_center'))
    var mapElement = document.getElementById("map");
    console.log({ mapElement });
    var mapTypeIds = [];
    for (var type in google.maps.MapTypeId) {
      mapTypeIds.push(google.maps.MapTypeId[type]);
    }
    mapTypeIds.push("OSM");

    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('loapmmmap');

      console.log(resp);
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      console.log(this.latitude);
      console.log(this.latitude);
      console.log(this.longitude);
      // this.map = new google.maps.Map(mapElement, {
      //   center: new google.maps.LatLng(this.latitude, this.longitude),

      //   // center: { lat: (this.latitude), lng: parseFloat(this.longitude) },
      //   zoom: 11,
      //   mapTypeId: "OSM",
      //   mapTypeControlOptions: {
      //     mapTypeIds: mapTypeIds
      //   }
      // });
    })
    // if (this.latitude && this.longitude) {
    console.log(this.latitude);
    console.log(this.longitude);
    this.map = new google.maps.Map(mapElement, {
      center: new google.maps.LatLng(this.latitude, this.longitude),

      // center: { lat: parseFloat(this.latitude), lng: parseFloat(this.longitude) },
      zoom: 11,
      mapTypeId: "OSM",
      mapTypeControlOptions: {
        mapTypeIds: mapTypeIds
      }
    });

    this.map.mapTypes.set("OSM", new google.maps.ImageMapType({

      getTileUrl: function (coord, zoom) {
        console.log({ coord });
        console.log('lat', this.map.center.lat());
        this.latitude = this.map.center.lat()
        console.log('long', this.map.center.lng());
        this.longitude = this.map.center.lng()
        // this.getAddressFromCoords(map.center.lat(), map.center.lng())
        return `http://apis.mapmyindia.com/advancedmaps/v1/hghezprcj7tvyo3nup21vb4sksf457al/still_image?center=${this.latitude},${this.longitude}&zoom=18&size=800x480&ssf=1&markers=${this.latitude},${this.longitude}|28.596000,77.225600>`;
      },
      tileSize: new google.maps.Size(256, 256),
      name: "OpenStreetMap",
      maxZoom: 18
    }));
    // }


  }

  ngAfterViewInit() {
    // this.loadMap();

  }


  // loadMap() { 
  //   console.log('loapmap');
  //   this.geolocation.getCurrentPosition().then((resp) => {
  //     console.log('loapmmmap');

  //     this.latitude = resp.coords.latitude;
  //     this.longitude = resp.coords.longitude;

  //     let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
  //     let mapOptions = {
  //       center: latLng,
  //       zoom: 15,
  //       mapTypeId: google.maps.MapTypeId.ROADMAP
  //     }

  //     this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
  //     console.log(this.mapElement.nativeElement);
  //     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  //     this.position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude)

  //     this.addMarker(this.map);

  //     this.map.addListener('dragend', () => {
  //       console.log(this.marker);
  //       this.latitude = this.map.center.lat();
  //       this.longitude = this.map.center.lng();
  //       this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
  //     });

  //   }).catch((error) => {
  //     console.log('Error getting location', error);
  //   });
  // }

  // getAddressFromCoords(lattitude, longitude) {
  //   console.log("getAddressFromCoords " + lattitude + " " + longitude);
  //   if (lattitude && longitude) {
  //     console.log(lattitude, longitude);
  //   }
  //   console.log('infor clicked');
  //   let options: NativeGeocoderOptions = {
  //     useLocale: true,
  //     maxResults: 5
  //   };
  //   console.log({ options });
  //   this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
  //     .then((result: NativeGeocoderResult[]) => {
  //       this.address = "";
  //       let responseAddress = [];
  //       for (let [key, value] of Object.entries(result[0])) {
  //         if (value.length > 0)
  //           responseAddress.push(value);

  //       }
  //       responseAddress.reverse();
  //       for (let value of responseAddress) {
  //         this.address += value + ", ";
  //       }
  //       this.address = this.address.slice(0, -2);
  //     })
  //     .catch((error: any) => {
  //       this.address = "Address Not Available!";
  //     });

  // }


  // addMarker(map: any) {
  // }



}
